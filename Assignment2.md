<a id="markdown-assignment-2" name="assignment-2"></a>
# Assignment 2
Sao Sheku Kanneh, 3788996

## Q1

$p=89,q=53,g=8537$

For each user the votes are calculated using their private number $r$ using $c=$

First the Voting Authority (VA) chooses two prime numbers $p=89,q=53$
Then the VA computes $n=p\cdot q= 89 \cdot 53=4717$
Then the VA selects an integer $g=8537$
Finally, the VA sends the public keys $(n=4717,g=8537)$ to the voting booths

The VA then generates the private key as follows:
The VA computes $\lambda=lcm(p-1,q-1)=lcm(88,52)=1144$
The VA computes $k=L(g^{\lambda}\bmod n^2)$ where $L(u)=\frac{u-1}{n}$, and $g^{\lambda}\bmod n^2=8537^{1144}\bmod 22250089=17754789$ therefore $k=\frac{17754788}{4717}=3764$
The VA computes private key parameter $\mu-k^{-1}\bmod n = 3764^{-1}\bmod 4717=4029$
Finally, the VA saves private key $\lambda=1144,\mu=4029$.


The voting booth encrypts the vote of each voter as follows:

For Voter 1:

$$
\begin{gathered}
c&=&g^mr^n\bmod n^2 \\
&=&8537^8\cdot 71^{4717}\bmod 22250089 \\
&=&16266423
\end{gathered}
$$

For Voter 2:

$$
\begin{gathered}
c &=&8537^8\cdot 72^{4717} \bmod 22250089 \\
&=& 5257843
\end{gathered}
$$

For Voter 3:

$$
\begin{gathered}
c &=&8537^8\cdot 73^{4717} \bmod 22250089 \\
&=& 15034916
\end{gathered}
$$

For Voter 4: 

$$
\begin{gathered}
c &=&8537^1\cdot 74^{4717} \bmod 22250089 \\
&=& 5594182
\end{gathered}
$$

For Voter 5: 

$$
\begin{gathered}
c &=&8537^1\cdot 75^{4717} \bmod 22250089 \\
&=& 1914786
\end{gathered}
$$

For Voter 6:

$$
\begin{gathered}
c &=&8537^1\cdot 76^{4717} \bmod 22250089 \\
&=& 16467987
\end{gathered}
$$

For Voter 7: 

$$
\begin{gathered}
c &=&8537^1\cdot 77^{4717} \bmod 22250089 \\
&=& 7009734
\end{gathered}
$$

The Voting Booth then sends these encrypted values to the Voting Server (VS) to compute the votes. The set of cipher-texts is $\{16266423,5257843,15034916,5594182,1914786,16467987,7009734\}$

The VS multiplies all the cipher-texts to tally the votes $C=16266423*5257843*15034916*5594182*1914786*16467987*7009734=1590008954297447998623725509907789802506604079584$ then sends the encrypted vote count to the VA

The VA decrypts $C$ as follows:

$$
\begin{gathered}
m&=&L(C^{\lambda}\bmod n^2)\cdot \mu \bmod n \\
&=&L(1590008954297447998623725509907789802506604079584^{1144}\bmod 22250089)\cdot 4029 \bmod 4717 \\
&=&L(7632107)\cdot 4029 \bmod 4717 \\
&=&1618 \cdot 4029 \bmod 4717 \\
&=&6518922\bmod 4717\\
&=&28
\end{gathered}
$$

The binary form of $m=011100$ shows that there are 3 votes for yes, and 4 votes for no

## Q2

### Q2.1 [RSA Signature Scheme]

$m=123456,p=5563,q=3821,e=9623$

In order to sign his messages and secure the SMS's sent between he and Alice, he must first compute the public and private keys

Bob generates the public key as follows:

First he computes $n=p\cdot q=5563\cdot3821=21256223$. 
He then computes $\phi(n)=(p-1)\cdot(q-1)=5562\cdot3820=21246840$. Given that his chosen paremeter $e=9623$ is co-prime to $\phi(n)$, Bob has successfully computed the public key $(n=21256223,e=9623)$ and he sends it to Alice.


Having generated and sent the public key, Bob generates the private key as follows:


Let $d$ be the private key, $d \cdot e=1 \bmod \phi(n)$

$$
\begin{matrix}
d\cdot9623 & = &1\bmod21246840 &\\
d  &=&9623^{-1}\bmod 21246840 & \\
d&=&16477727 \\
\end{matrix} 
$$

Having generated the private key $d=16477727$, Bob signs his message $m=123456$ as follows: $s=m^d\bmod n  = 123456^{16477727}\bmod{21256223}=3047900$, then Bob sends $(m=123456,s=3047900)$ to Alice.

To verify that this message is from Bob, Alice uses the public key $(n=21256223,e=9623)$ as follows: $m'=s^e\bmod n =3047900^{9623}\bmod 21256223=123456$. Thus, Alice confirms that it was Bob who signed and sent this message to her.

<a id="markdown-q22-elgamal-signature" name="q22-elgamal-signature"></a>
### Q2.2 [ElGamal Signature]

$m=4567,p=7331,g=3411,x=41$

For Bob to be able to sign his message and for Alice to be able to verify Bob's message, Bob must first generate the public and private keys.

Bob first generates the public key as follows:

Bob must first generate a public key paraemeter $y=g^x\bmod p=3411^{41}\bmod7331=6162$
With the parameter $y$ generated, Bob has completed and sends the public key $(p=7311,g=3411,y=6162)$ to Alice who will later use it for verification.

Bob now signs the message as follows:

First Bob selects a random number $k$ such that $1\leq k\leq p-2$ which also satisfies $\gcd{(k,p-1)}=1$. Bob determines $k=3229$ satisfies these requirements.
Now Bob computes the signature parameters $s$ and $r$ as follows:

$$
\begin{gathered}
r  &= & g^k\bmod p \\
& = & 3411^{3229}\bmod 7331 \\
&=&6379 \\
\\
s &= &k^{-1}(m-x\cdot r)\bmod(p-1) \\
& = & 3229^{-1}(-256972)\bmod 7330 \\
& = & -218169228 \bmod 7330 \\
& = & 892
\end{gathered}
$$

Bob sends the signed message $(m=4567,r=6379,s=892)$

Upon receiving the signed message from Bob, Alice verfiies that the message is from Bob as follows:

Alice checks $r\geq 1 \Longleftrightarrow 6379\geq 1$ and $r \leq p-1 \Longleftrightarrow 6379 \leq 7330$. Given that both conditions are true, Alice accepts the signature.
Next Alice computes verification parameters $v$ and $w$ as follows: 

$$
\begin{gathered}
v & = & g^m\bmod p \\
& = & 3411^{4567} \bmod 7331 \\
& = & 548 \\
\\
w & = & y^r\cdot r^s\bmod p \\
& = & 6162^{6379}\cdot6379^{892}\bmod 7331 \\ 
& = &   548
\end{gathered}
$$

Since $v=w=548$, Alice confirms that the message was sent by Bob.

### Q2.3 [RSA Signature Scheme for Text Message]



Bob's message $M$ yields a hash $h(M)=1b435d0ebce43e91b0d6a94c0f5e9564$, which when converted into an integer gives $m=36238927207461089232091882603423896932$ which is the message to sign

In order to sign this message and its hash Bob must first generate the public and private keys. 

Bob generates the public key as follows: 

Bob first selects two random primes 

$p=278966591577398076867954212605012776073$

$q=467207331195239613378791200749462989467$

Then

$$
\begin{gathered} 
n &=&p\cdot q \\
&=&278966591577398076867954212605012776073\cdot 467207331195239613378791200749462989467\\
&=&130335236743508564903180077368625855000210410960209719604451926425399228623091
\end{gathered}
$$

Next Bob calculates

$\phi(n)=(p-1)\cdot(q-1)$
$=130335236743508564903180077368625854999464237037437081914205181012044752857552$

Next Bob selects a prime number $e$ such that $e$ is co-prime to $\phi(n)$. Bob chooses 
$e=41$

Bob sends the public key 

$$
(n=130335236743508564903180077368625855000210410960209719604451926425399228623091, e=41)
$$
 to Alice

With the public key generated, Bob generates the private key $d$ as follows: 

$$
\begin{gathered}
d\cdot e = 1\bmod \phi(n) \\
= 41^{-1}\bmod 130335236743508564903180077368625854999464237037437081914205181012044752857552\\
=50862531412100903376850761899951553170522629087780324649445924297383318188313\\
\end{gathered}
$$


Next Bob signs the message as follows 


$s=m^d\bmod n$


$$
\begin{gathered}
&=&36238927207461089232091882603423896932^{50862531412100903376850761899951553170522629087780324649445924297383318188313}\bmod 130335236743508564903180077368625855000210410960209719604451926425399228623091\\
&=&126798804286385130870848966135941566606057839336135951340495096277825470279796
\end{gathered}
$$

Finally, Bob sends the message and signature $(M,s)$ to Alice.

Alice, upon receiving the message, verifies the signature as follows:

$m'=s^e\bmod n$

$$
\begin{gathered}=126798804286385130870848966135941566606057839336135951340495096277825470279796^{41}\bmod 130335236743508564903180077368625855000210410960209719604451926425399228623091 \\
=36238927207461089232091882603423896932
\end{gathered}
$$

Alice verifies $h(M)=m=m'=1b435d0ebce43e91b0d6a94c0f5e9564$ and accepts the message from Bob. 

## Q3

### I
![RSA Private Key](img/rsa_priv.png)
![RSA Public Key](img/rsa_pub.png)

### II

Alice selects secret key $K_{ab}=86524$


![aes plaintext](img/aes_plain.png)

### III

Alice uploads `ciphertext.txt` to ipfs and receives `UI=QmabL2o3HdqnoP1hUU923Ha5Vr71UCTqrypQ9VXDWLVHZS`

![ipfs upload](img/ipfs_add.png)

### IV

![encrypted key send](img/encrypted_key.png)

### V

Alice sends the `encrpted-key.txt` to Bob

### VI

![decrypted key](img/decrypted_key.png)

### VII

![received encrypted message](img/ipfs_download.png)

### VIII

![aes decrypt](img/aes_decrypt.png)

## Q4.3

My privacy-preserving online voting system is a a web-based application developed in Javascript.

### What it is and how to use it

Before you can run the application you must make sure you have [postgres](https://www.postgresql.org/download/) and [npm](https://www.npmjs.com/get-npm) installed on your system.

To run the application:

1. Clone the repo: `git clone git@github.com:ItsThatBlackKid/SenateVote.git`

2. Enter the repo directory `cd SenateVote/` and run `npm i`. This will download all the dependencies for the server. 

3. Using postgres, create a database named "senatevote" then create the votes table  
    3.1 `createdb senatevote`
    3.2 `psql -d senatevote`
    3.3 `create table vote (votes varchar(800) NOT NULL)`
(I'm sorry, the steps to setup postgres are too long to post here)

4. Enter the `voter` directory and install the dependencies
    4.1 `cd voter`
    4.2 `npm i`

5. Return to the root directory and run the server
    5.1 `cd ../`
    5.2 `npm start`

6. Enter ther `voter` directory again and run the frontend. A new tab will open in your preferred browser which is where the GUI is found.
    6.1 `cd voter`
    6.2 `npm start`


The application allows users to to choose between to options upon loading: Voting Authority, or Voting Booth.

The Voting Authority option takes users to the voting authority page where there user is prompted to enter a username and password to be able to count votes. This part of the application was intentionally buit to look bland as the majority of its functionality happens under the hood.

To login, one can simply enter "vauthority" username and "rootadmin" as the password. Once logged in, users may simply press "count votes" 
and the application wil query the server for the total votes.

The Voting Booth option takes users to the voting booth page where they are presented with two "candidates" to vote for.
When the user clicks on a candidate a tick appears beneath that candidate's name. Next, the user must click "Make Vote" for their vote to be registered. Finally, when all votes are registered, the user must click "Submit Votes" to send the votes back to the server.

### Implementation Details


To implement the voting system I used javascript. More specificaly I used the javascript `nodejs` environment the following technologies: 

* [ReactJS](https://www.npmjs.com/package/react)
* [Express](https://www.npmjs.com/package/express)

ReactJS was used in the frontend, allowing me to quickly develop the GUI.

Express was used in the backend, allowing me to develop a server that was responsible for generating and sending the public and private key parameters
to frontend.

While building the system I ran into a major issue selecting the public and private key parameters. I found that Javascript's basic `Number` data type was limited to 64-bit double-precision values. Under any other circumstance this would've been a comfortable range to work with. Unfortunately, due to need to multiply and power large numbers I found that my parameters were easily exceeding this 64-bit limit before the server could even finish starting. To circumvent this issue I tried using Javascript's `BigInt` data type but was also met with another problem: there was no way to reliably perform arithmetic calculations with `BigInt`. Though the standard operators `*` (multiplication), `**`(exponentiation), `-` (subtraction), `+` addition, and `/` (division) were still available, I found that at times Javascript was not properly computing these results. Finally, I found a library named [big-integer](https://www.npmjs.com/package/big-integer) which is a wrapper around the `BigInt` data type and provides many math operators and functions beyond the standard provided by Javascript that proved to make development much easier and thus resulting in cleaner code. All `BigInt` variables in my code were handled using this library, with all of them being instantiated as so `varname = bigInt()`.

The most complicated part of the system was writing the logic for generating the public and private keys. In the file `/crpyto/crpyto.js` there is a single function `generateParemeters`, and this function is where all of the magic happens. To build a working system that relied on Paillier Encryption, I had to try and translate the mathematical rules and steps of Paillier into functioning code. Each step is broken down into logical conditions that if not met, will cause `generateParameters` to run recursively until that is the case. This is what the function looks like:

The primes $p$ and $q$ are selected random: 

```
let p = bigInt();
let q = bigInt();
let n = bigInt();
let g = bigInt();
let q = bigInt();

while (!p.isPrime()) {
        p = bigInt.randBetween("2", "1e10");
}

while (!q.isPrime()) {
    q = bigInt.randBetween("2", "1e10");
}

// p cannot equal q 
if(p.equals(q)) {
    generateParameters();
}
```

Then $\phi(n)=(p-1)\cdot(q-1)$ is computed:
```
const phi = p.subtract(1).multiply(q.subtract(1));
```

And $n=p\cdot q$:
```
if (bigInt(1).equals(bigInt.gcd(phi, p.multiply(q)))) {
    n = p.multiply(q);
    console.log(n);
    if (n === 0) {
            generateParameters();
    }
} else {
    generateParameters();
}
```

Then $g$:
```
while (!bigInt(1).equals(bigInt.gcd(g, n.square()))) {
       g =bigInt.randBetween("2", "1e10");
}
```
$g$ was the harder to compute as the instructions in the lecture notes were unclear. However I found Math Exchange that g must simply be a value such that $gcd(g,n)=1$ and $gcd(k,n)=1$, $k$ is computed further down.

Next, we compute $\lambda,\mu, k$:
```
const lambda = bigInt.lcm(p.minus(1), q.minus(1));
console.log(n.square());
const nsquare = n.square();
const u = g.modPow(lambda, nsquare);
console.log(u);
const lu = u.subtract(1);
console.log(lu);
const k =  lu.over(n);
console.log(bigInt.gcd(k,n));
const mu = k.modInv(n);
```

In the voting booth, votes are encrypted and added to an array. When all votes are counted, the booth sends this array of encrypted values to the server: 

```
// counting votes
const r = bigInt.randBetween("1", "400");
const n = bigInt(sessionStorage.getItem("n"));
console.log(n);
const g = bigInt(sessionStorage.getItem("g"));

const num = this.state.vote;
console.log(num);
console.log(g.modPow(num,n.square()));
console.log(r.modPow(n,n.square()));
const c = g.modPow(num, n.square()).multiply(r.modPow(n, n.square()));
this.setState({
    votes: this.state.votes.concat([c]),
    vote: 0
})

 // sending votes
if(this.state.votes.length === 0) {
    return;
}

client.vote.makeVote(this.state.votes).then((res) => {
    this.setState({
        message: "Vote submitted",
    })
})

```
And the server computes these votes and adds them to the database: 

```
postgres.query(`SELECT * from vote`, (error, vote) => {
    if (error) {
        res.status(500).json({error: "error"});
        console.log(error);
        return;
    }
    let c = bigInt(1);
    const votes = req.body.votes;
    votes.forEach((vote) => {
        c = bigInt(vote).multiply(c);
        console.log(vote);
        console.log(c);
    });
    const n = bigInt(req.app.locals.parameters.n);
    c = c.mod(n.square());
    if (vote.rows[0]) {
        c = bigInt(vote.rows[0].votes).multiply(c).mod(n);
        postgres.query(`UPDATE vote set votes = ${c.toString()}  WHERE votes = '${vote.rows[0].count}'`).then((v, error) => {
            console.log(v);

        }).catch((error) => {
            console.log(error);
            res.status(500).json({err: "Error"});
        })
        } else {

            postgres.query(`INSERT INTO vote (votes) values (${c.toString()}) `).then((error, v) => {
                if (error)
                    console.log(error);

        }).catch((err) => {
            console.log(err);
            res.status(500).json({err: "Error"});
        })
    }

    })
}
```

The voting authority receives the counted votes and decrypts: 

```
countVotes = () => {
    axios.get('/votes/count').then((res) => {
        const lambda = bigInt(res.data.lambda);
        const mu = bigInt(res.data.mu);
        const n = bigInt(res.data.n);
        const c = bigInt(res.data.c);

        // m = L(c^(lambda) mod n^2)*mu mod n
        const lu = c.modPow(lambda,n.square()).minus(1).divide(n);
        const m = lu.multiply(mu).mod(n);

        const mProper = m.toString(2);
        const mLength = mProper.length;
        const bits =  6 - mLength;
        let nZeroes = "";

        if(bits > 0) {
            for(let i = 0; i < bits; i++) {
                nZeroes = "0" + nZeroes;
            }
        }

        let binary = nZeroes + mProper.toString(2);

        const alice = binary.substring(0,3);
        const bob = binary.substring(3,binary.length);

        const aliceCount = parseInt(alice,2);
        const bobCount = parseInt(bob,2);

        this.setState({
            votesCalculated: true,
            aliceCount: aliceCount,
            bobCount: bobCount,
        })

    })
};
```
## Q5

### Scenario 1

![mim attack](img/mim_attack.png)

1. Trudy sits between Alice and Bob, pretending to be Alice Trudy copies Alice's message and sends it to Bob.
2. Bob sends back to Trudy a random number $R_B$ which Trudy records.
3. Trudy opens a second session with Bob and sends $\text{"Alice", }R_B$ to Bob.
4. Bob encrypts sends the encrypted version of $R_B$ and a new random number $R_C$ to Trudy.
5. Trudy ignores $R_C$ and sends $E(R_B,K_AB)$ to Bob who accepts it

### Scenario 2

![mim attack 2](img/mim_attack2.png)

1. Trudy sits between Alice and Bob as before, copies Alice's message and sends it to Bob.
2. Bob sends back to Trudy (pretending to be Alice) $R_B,E(R_A,K_AB)$
3. Trudy records Bob's and sends it to Alice.
4. Alice sends back the encrypted $E(R_B,K_AB)$ to Trudy
5. Trudy sends this to Bob who accepts it.