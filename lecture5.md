# Lecture 5: Privacy Preseveration Computation


## What is *data* privacy and types of Data Privacy

* <span style="color:darkred">Information Privacy</span> (also known as data privacy) is the privacy of personal information and usually relates to personal data stored on computer systems. It relates to different data types, including:
  * <span style="color:darkred">Internet Privacy  (online privacy) </span>: All personal dat shared over the internet is subject to privacy issues. Most websites publish a privacy policy that details the website's inteded use of collected online and/or offline collected data.
  * <span style="color:darkred">Financial Privacy</span>: Financial information is particuarly sensitive, as it may easily be used to cmmit online and/or offline fraud
  * <span style="color:darkred">Medical Privacy</span>: All medical records are subejct to stringent laws that address user access privileges. By law, security and authentication systems are often required for individuals that process and store medical records

### How can Data Privacy be applied? 

Information privacy may be applied in nuemrous ways, including encryption, authentication, and data masking

## Basic Building Blocks for Privacy Preserving Computation
* (+) Adding encrypted numbers securely: 
  * A sender encrypts two numbers with a generated public key and sends the encrypted numbers to the cloud. The cloud multiplies (x) the encrypted numbers to perform additoin and sends the result to the receiver who decrpyts it.
* (x) Multiplying encrypted numbers securely:
  * A sender encrypts two numbers with a generated public key and sends the encrypted numebrs to the cloud. Cloud multiplies encrypted numbers to perform multiplication (x) and sends the results to the receiver who decrypts it.

* Partially homorphic
  * RSA Homomorphism: Multiplying numbers secretly
  * Paillier Homorphism: Adding numbers secretly
  * ElGamal Homomorphism: Multiplying numbers secretly  
* Fully homomorphic: add and multiply numbers secretly
* Pros and cons: 
  * partially homomorphic schemes are faster, but limited in their capabilities - cannot add and multiply simultaenously
  * Fully homomorphic schemes can do both simultaneously but are very slow

## RSA Homomorphism: Multiplying two numbers secretly

* Say the sender encrypts two messages <span style="color:darkred">$M_1=3$</span> and <span style="color:darkred">$M_2=4$</span> with public key: <span style="color:darkred">$n=33,e=7$</span> and private key <span style="color:darkred">$d=3$</span> as follows:<span style="color:darkred">$C_1=M_{1}^e\bmod n = 3^7\bmod33=2187\bmod33=9$</span>
<span style="color:darkred">$`C_2=M_2^e\bmod n=4^7\bmod33=16384\bmod33=16`$</span>
* Sends sends <span style="color:darkred">$C_1=9$</span> and <span style="color:darkred">$C_2=16$</span> to the cloud to multiply the numbers.
* The cloud finds that <span style="color:darkred">$C_1\cdot C_2=144$</span>. It sends the multiplied value to receiver
* Receiver decrypts <span style="color:darkred">$144^3\bmod33=12$</span> which is <span style="color:darkred">$M_1\cdot M_2=3\cdot4=12$</span>

## ElGamar Homomorphism: Multiplying two numbers secretly

Alice (the sender) has two message <span style="color:darkred">$m_1=5,m_2=6$</span>. She wants to **multiply** the messages securely using <span style="color:darkred">Homomorphic</span> properties of ElGamal cryptosystem and send to Bob (the receiver). The server will perform the multiplication and send the encrypted results to Bob. Bob should find 12 after perfmoring decryption. Bob chooses public parameters <span style="color:darkred">p=2879,g=2585</span> and private key <span style="color:darkred">$x=47$</span>. Alice chooses two random numbers $r_1=154,r_2=96$ to encrypt the two messages.

### Step 1 & 2

* Receiver generates <span style="color:darkred">$p=2879$</span> and  <span style="color:darkred">g=2585</span> and chooses a private key <span style="color:darkred">$x=47$</span>
* Receiver then computes <span style="color:darkred">$y=g^x\bmod p=2585^{47}\bmod2879=2826$</span>
* Receiver then sends <span style="color:darkred">$p,g,y$</span> to sender

### Step 3 & 4

* Sender chooses two random numbers <span style="color:darkred">$r_1=154,r_2=96$</span>
* Sender calculates: <span style="color:darkred">$k_1=y^{r_1}\bmod p = 2826^{154}\bmod2879=2343$</span> and <span style="color:darkred">$k_2 = y^{r_2}\bmod p=2826^{96}\bmod 2879=1845$</span>
* Sender calculates <span style="color:darkred">$C_1,C_2$</span> and two messages <span style="color:darkred">$m_1=5,m_2=6$</span> as follows: <span style="color:darkred">$C_{11}=g^{r_1}\bmod p = 2585^{154}\bmod2879=1309$</span> <span style="color:darkred">$C_{12}=(m_1\cdot k_1)\bmod p =(5\cdot2343)\bmod2879=199$</span> <span style="color:darkred">$C_{21}=g^{r_2}\bmod p = 2585^{96}\bmod2879=1138$</span> <span style="color:darkred">$C_{22}=(m_2\cdot k_2)\bmod p =(6\cdot1845)\bmod2879=2433$</span>
* Sender sends <span style="color:darkred">$(C_{11},C_{22}),(C_{21},C_{22})$</span> to cloud server.

### Step 5 & 6
* Cloud server computes <span style="color:darkred">A</span> and <span style="color:darkred">B</span> as follows: <span style="color:darkred">$A=(C_{11}\cdot C_{21})\bmod p =(1309\cdot1138)\bmod2879=1199$</span> <span style="color:darkred">$B=(C_{12}\cdot C_{22})\bmod p =(1271\cdot1622)\bmod2879=495$</span>
* Cloud server then sends <span style="color:darkred">A</span> and <span style="color:darkred">B</span> to receiver
### Step 7:

* Receiver computes the result <span style="color:darkred">$M=m_1\cdot m_2$</span> as follows: <span style="color:darkred">$M=\dfrac{B\bmod p}{A^x\bmod p}=\dfrac{495\bmod2879}{1199^{47}}=\dfrac{495\bmod2879}{1456\bmod2879}=(495\bmod2879)\cdot(1456^{-1}\bmod2879)=(495\cdot698)\bmod2879=30$</span>
* The final result is $M=30$

## Paillier Homomorphism: Adding two numbers secretly


### Paillier Homomorphism: Encrypting the messages
<aside style="float:right;position:relative">

<span style="color:darkred">$\text{Here, }\lambda = lcm(p-1,q-1)\text{ } \mu=k^{-1}\bmod n \text{where, } k=L(g^{\lambda}\bmod n^2)$ </span>


</aside>

* Say, the sender encrypts two messages <span style="color:darkred">$M_1=4,M_2=1$</span> with parameters: <span style="color:darkred">$p=5,q= 7,n=35,\lambda=12,g=164,\mu=23$</span> as follows: <span style="color:darkred">$C_1=416 \text{ using } r=6$ $C_2=127 \text{ using } r=17$</span> Where <span style="color:darkred">$C_{m_x}=g^mr^n\bmod n^2$</span>


### Paillier Homomorphism: server computing the sum

* To add the numbers the server Multiplies <span style="color:darkred">$C_1=416, C_2=127$</span> and finds that  <span style="color:darkred">$C_1\cdot C_2=416\cdot 127=52832$</span>. This value is sent to the receiver.

### Paillier Homomorphism: decrypting the message

* The receiver decrypts<span style="color:darkred">$C=52832$</span> using <span style="color:darkred">$m=L(c^{\lambda}\bmod n^2)\cdot\mu\bmod n=L(52832^{12}\bmod35^2)\cdot23\bmod35=5$</span> where <span style="color:darkred">$L(u)=\dfrac{(u-1)}{n}$</span>
