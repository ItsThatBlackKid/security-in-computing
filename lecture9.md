# Lecture 9: Digital Authentication & Security Protocols

## Authentication
Are you who you say you are?

* Determine whether access is allowed
* Authenticate human to machine
* Or, possibly machine to machine

### Authentication vs Authorization
* Two parts to access control...
* Authentication: are you who you say you are?
  * (see above)
* **Authorisation**: Are you allowed to do that?
  * Once you have access, what can you do?
  * Enforces limits on actions
  * TO enforce actions we also need intrustion detectoin

### Best practice 

* Involves making sure that the chosen security approach leverages layered capabilities
* If a defence measure fials to detect an attack, another measure is available to help prevent the attack (or further damage)
* So, we need both Authentication and Authorisation (including intrusion detection/ prevention)
